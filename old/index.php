<?php

/**
 * Reimagine what you expect...
 */
$app->get('/', function() {
	return ['version' => '5.3'];
});

/**
 * From your micro-framework...
 */
$app->post('framework/{id}', function($framework) {

	$this->dispatch(new Energy($framework));
});
?>