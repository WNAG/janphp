<?php

class DBConnet
{
    
    public $servername_ = "localhost";
    public $username_ = "root";
    public $password_ = "";
    public $dbname_ = "print_order";

    public function __construct(){
        date_default_timezone_set('Asia/Taipei');
        
    }
    function linkDB(){
        // Create connection
        /*$db = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        // Check connection
        if ($db->connect_error) {
            return "error";
        } 
        $db->query('set names utf8');*/

        // Connect to CloudSQL from App Engine.
        
        $dsn = getenv('MYSQL_DSN');
        $user = getenv('MYSQL_USER');
        $password = getenv('MYSQL_PASSWORD');
        if (!isset($dsn, $user) || false === $password) {
            //throw new Exception('Set MYSQL_DSN, MYSQL_USER, and MYSQL_PASSWORD environment variables');
            //$db = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
            $db = new PDO("mysql:host=localhost;dbname=print_order", "root", "");
        }else{
            $db = new PDO($dsn, $user, $password);
        }
    
        
        $db->query('set names utf8');
        
        return $db;
    }

    //新增訂單
    function insertOrder($data, $money, $orderNumber,$customerName,$customerPhone){
        //回傳值
        $rqAry = Array(
            'type' => 'hi',
            'msg' => "hello"
        );

        $conn = $this->linkDB();

        if ($conn == "error") {
            $rqAry['type'] = 'error';
            $rqAry['msg'] = 'linkDB error';
            return $rqAry;
        }

        $data = addslashes($data);

        $datetime= date("Y/m/d H:i:s");
        $sql = "INSERT INTO order_list (data, money, date, order_number, status, customer_name, customer_phone) VALUES ('$data', $money ,  '$datetime', '$orderNumber', 'new', '$customerName', '$customerPhone')";

        if($conn->query($sql)){
            $rqAry['type'] = 'complete';
            $rqAry['msg'] = 'ok';
        }else{
            $rqAry['type'] = 'error';
            $rqAry['msg'] = 'sql query error data: ' . $data . ' money: ' . $money ;
        }
        
        return $rqAry;
        
    }
    //取得今日訂單數量
    function getTodayOrderCount(){
        $conn = $this->linkDB();

        if ($conn == "error") {
            return -1;
        }

        $sql = "SELECT COUNT(*) AS Count FROM `order_list` WHERE date_format(date,'%Y-%m-%d') = CURDATE()";
    
        //$result = $conn->query($sql);

        foreach ($conn->query($sql) as $row) {
            return $row["Count"];
        }
        /*
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                return $row["Count"];
            }
        } else {
            return 0;
        }*/
    }
    //取得今日訂單
    function getTodayOrder(){
        $conn = $this->linkDB();

        if ($conn == "error") {
            return -1;
        }

        $sql = "SELECT * FROM `order_list` WHERE date_format(date,'%Y-%m-%d') = CURDATE()";
    
        //$result = $conn->query($sql);

        $rows = array();

        foreach ($conn->query($sql) as $row) {
            $rows[] = $row;
        }
        /*while($r = mysqli_fetch_assoc($result)) {
            $rows[] = $r;
        }*/
        echo json_encode($rows);
    }
}
?>