<?php



use Slim\Http\Request;
use Slim\Http\Response;
use JaN\OrderManager;


require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../classes/orderManager.php';

// Routes

$container = $app->getContainer();

$orderManager = NULL;
function getOrderManager($db){
    if (!isset($orderManager)) {
        $orderManager = new OrderManager($db);
    }
    return $orderManager;
}

$container['orderManager'] = 

$app->get('/name/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->post('/test', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");
    $body = $request->getBody();
    //echo $body;
    $response->write($body);
    return $response;
    // Render index view
    //return $response->getBody();
    //return $this->renderer->render($response, 'test.php', $args);
});

//取得訂單
$app->post('/getorder/{type}', function (Request $request, Response $response, array $args) {
    return getOrderManager($this->db)->getList($args['type']);
});
$app->get('/getorder/{type}', function (Request $request, Response $response, array $args) {
    return getOrderManager($this->db)->getList($args['type']);
});

//增加訂單
$app->post('/addorder', function (Request $request, Response $response, array $args) {
    return getOrderManager($this->db)->addOrder($request->getBody());
});

//編輯訂單
$app->post('/editorder', function (Request $request, Response $response, array $args) {
    return getOrderManager($this->db)->editOrder($request->getBody());
});

//取得是否有新通知
$app->get('/getnew/{time}', function (Request $request, Response $response, array $args) {
    return getOrderManager($this->db)->getNew($args['time']);
});

//顯示目前時間
$app->get('/now', function (Request $request, Response $response, array $args) {
    return date("Y/m/d H:i:s");
});