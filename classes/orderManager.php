<?php

namespace JaN;

use PDO;

//取得訂單元件
class OrderManager
{
    protected $db;

    public function __construct($db) {
        $this->db = $db;

        //時區
        date_default_timezone_set("Asia/Taipei");
    }
    public function addOrder($data) {        

        //今日訂單數量
        $sql = "SELECT COUNT(*) AS Count FROM `order_list` WHERE date_format(date,'%Y-%m-%d') = CURDATE()";

        $count = 0;

        foreach ($this->db->query($sql) as $row) {
            $count = $row["Count"];
        }
        //=====================
        $dataJson = json_decode($data,true);
        //$dataJson = $data;

        $orderNumber = $count + 1;
        //
        $orderNumberStr = str_pad($orderNumber,3,'0',STR_PAD_LEFT);
        $orderNumberStr = date("md") . $orderNumberStr;

        $rqAry = Array(
            'type' => 'hi',
            'msg' => "hello",
            'order_number' => $orderNumberStr
        );

        // $data = addslashes($data);

        $money = $dataJson['money'];
        $datetime= date("Y/m/d H:i:s");
        $customerName = addslashes($dataJson['customerName']);
        $customerPhone = addslashes($dataJson['customerPhone']);

        // if ($conn == "error") {
        //     $rqAry['type'] = 'error';
        //     $rqAry['msg'] = 'linkDB error';
        //     return $rqAry;
        // }
        $data = addslashes($data);

        $sql = "INSERT INTO order_list (data, money, date, order_number, status, customer_name, customer_phone, last_date) VALUES ('$data', $money ,  '$datetime', '$orderNumberStr', 'new_unpaid', '$customerName', '$customerPhone', '$datetime')";
        
        if($this->db->query($sql)){
            $rqAry['type'] = 'complete';
            $rqAry['msg'] = 'ok';
        }else{
            $rqAry['type'] = 'error';
            $rqAry['msg'] = 'sql query error data: ' . $data . ' money: ' . $money ;
        }
        
        return json_encode($rqAry);

    }

    public function editOrder ($data){
        
        $dataJson = json_decode($data, true);
        $money = $dataJson['money'];
        // echo $data;

        $orderIndex = $dataJson['order_index'];
        $status = $dataJson['status'];

        $date= date("Y/m/d H:i:s");

        $dataAry = Array(
            'data' => $dataJson['data']
        );
        $orderData = addslashes(json_encode($dataAry, JSON_UNESCAPED_UNICODE));
        
        // $sql = "INSERT INTO order_list (data, money, date, order_number, status, customer_name, customer_phone) VALUES ('$data', $money ,  '$datetime', '$orderNumber', 'new', '$customerName', '$customerPhone')";

        $sql = "UPDATE order_list SET data='$orderData',money=$money,status='$status',last_date='$date'  WHERE order_index=$orderIndex";
        $this->db->query($sql);

        
        $sql2 = "SELECT * FROM `order_list` WHERE order_index=$orderIndex";
        $requre ="";
        

        $rs = $this->db->query($sql2);
        if (is_object($rs)) {
            $rows = array();
            
            foreach ($rs->fetchAll(PDO::FETCH_OBJ) as $row) {
                $rows[] = $row;
            }
            
            $requre = json_encode($rows);
        }
        
    

        return $requre;
    }

    public function getList($type) {
        $requre = "";

        //預設取得全部
        $sql = "SELECT * FROM `order_list`";

        if (isset($type)) {
            switch ($type) {
                case 'today':
                    //取得今日資料
                    $sql = "SELECT * FROM `order_list` WHERE date_format(last_date,'%Y-%m-%d') = CURDATE()";
                    break;
                case 'three':
                    //三天內
                    $sql = "SELECT * FROM `order_list` WHERE DATE_SUB(CURDATE(), INTERVAL 3 DAY) <= date(last_date)";
                    break;
                case 'seven':
                    //七天內
                    $sql = "SELECT * FROM `order_list` WHERE DATE_SUB(CURDATE(), INTERVAL 7 DAY) <= date(last_date)";
                    break;
                case 'month':
                    //30天內
                    $sql = "SELECT * FROM `order_list` WHERE DATE_SUB(CURDATE(), INTERVAL 1 MONTH) <= date(last_date)";
                    break;
                default:
                    //指定編號
                    $sql = "SELECT * FROM `order_list` WHERE order_index=$type";
                    break;
            }
        }

        if ($sql != "") {
            $rows = array();
            $rs = $this->db->query($sql);
            foreach ($rs->fetchAll(PDO::FETCH_OBJ) as $row) {
                $rows[] = $row;
            }
            
            $requre = json_encode($rows);
        }

        return $requre;
    }

    //取得新項目，參數為前端最後更新時間
    public function getNew($lastTime)
    {
        $sql = "SELECT * FROM `order_list` WHERE last_date > '" . $lastTime . "'";

        $rows = array();

        foreach ($this->db->query($sql) as $row) {
            $rows[] = $row;
        }
        
        $requre = json_encode($rows);
        return $requre;
    }
}

?>